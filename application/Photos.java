package application;

import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/**
 * Photos application
 * @author Jon Cobi Delfin and Luis Guzman
 *
 */
public class Photos extends Application{
	
	@Override
	public void start(Stage primaryStage)
	throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/view/Login.fxml"));
		AnchorPane root = (AnchorPane)loader.load();
		Scene scene = new Scene(root);
		primaryStage.setScene(scene);
		primaryStage.setTitle("Photos - User Login");
		primaryStage.setResizable(false);
		
		primaryStage.show();
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
