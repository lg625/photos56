package controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Optional;
import model.Tag;

import javafx.fxml.*;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
/**
 * Controller for PhotoInfoView
 * @author Jon Cobi Delfin and Luis Guzman
 *
 */
public class PhotoInfoViewController {
	private model.Album album;
	private model.Photo photo;
	private Stage st; //Stage that contains window

	private int index;
	private ArrayList<Tag> oldTags;
	
	@FXML private ImageView photoIV;
	@FXML private Button prevBtn;
	@FXML private Button nextBtn;
	@FXML private TextField captionTF;
	@FXML private TextField dateTimeTF;
	@FXML private TextArea tagsTA;
	@FXML private Button editBtn;
	@FXML private Button confirmEditBtn;
	@FXML private Button cancelEditBtn;
	@FXML private Button deleteBtn;
	
	@FXML
	private void initialize() {
		
	}
	
	/**
	 * Pass data from PhotoViewsController to here
	 * @param a album
	 * @param p photo
	 * @param s stage
	 */
	public void initData(model.Album a, model.Photo p, Stage s) {
		album = a;
		photo = p;
		st = s;
		index = album.photos.indexOf(photo);
		
		photoIV.setImage(new Image(photo.getImageURL()));
		showImageData(photo);
	}
	
	/**
	 * Go back a photo
	 */
	public void prevPhoto() {
		if(album.numPhotos == 0) return;
		if(index > 0) index--;
		photo = album.photos.get(index);
		photoIV.setImage(new Image(photo.getImageURL()));
		showImageData(photo);
	}
	
	/**
	 * Go forward a photo
	 */
	public void nextPhoto() {
		if(album.numPhotos == 0) return;
		if(index < album.numPhotos-1) index++;
		photo = album.photos.get(index);
		photoIV.setImage(new Image(photo.getImageURL()));
		showImageData(photo);
	}
	
	/**
	 * Edit photo
	 */
	public void editPhoto() {
		//Save old data
		oldTags = photo.getTags();
		
		//Allow editing
		captionTF.setEditable(true);
		tagsTA.setEditable(true);
		confirmEditBtn.setVisible(true);
		cancelEditBtn.setVisible(true);
		
		editBtn.setVisible(false);
		deleteBtn.setVisible(false);
	}
	
	/**
	 * Confirm changes to photo
	 */
	public void confirmEdit() {
		//Get new fields
		String newCaption = captionTF.getText();
		if(newCaption.isEmpty()) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setContentText("Error: new caption is empty");
			alert.show();
			showImageData(photo);
			return;
		}
		String newTags[] = tagsTA.getText().split("[;\n]");

		//Remove old tags
		Iterator<Tag> it = oldTags.iterator();
		while(it.hasNext()) {
			it.next();
			it.remove();
		}
		
		//Set new data
		photo.setName(newCaption);
		for(String tag : newTags) {
			String name, value;
			name = tag.split(":")[0];
			value = tag.split(":")[1];
			Tag t = new Tag(name, value);
			photo.addTag(t);
		}
		
		showImageData(photo);
		
		captionTF.setEditable(false);
		tagsTA.setEditable(false);
		confirmEditBtn.setVisible(false);
		cancelEditBtn.setVisible(false);
		
		editBtn.setVisible(true);
		deleteBtn.setVisible(true);
	}
	/**
	 * Cancel editing photo
	 */
	public void cancelEdit() {
		showImageData(photo);
		
		//Allow editing
		captionTF.setEditable(false);
		tagsTA.setEditable(false);
		confirmEditBtn.setVisible(false);
		cancelEditBtn.setVisible(false);
		
		editBtn.setVisible(true);
		deleteBtn.setVisible(true);
	}
	
	/**
	 * Delete photo
	 */
	public void deletePhoto() {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setContentText("Are you sure you want to delete " + captionTF.getText() +"?");
		alert.setHeaderText("Confirmation");
		Optional<ButtonType> result = alert.showAndWait();
		if(result.get() == ButtonType.OK) {
			album.removePhoto(photo);
			album.numPhotos--;
			if(album.numPhotos > 0) {
				try{
					nextPhoto();
				} catch(Exception e) {
					try{
						prevPhoto();
					} catch(Exception e2) {
						//Shouldnt get here
					}
				}
			}else {
				st.close();
			}
		}
	}
	
	/**
	 * Change the data fields
	 */
	private void showImageData(model.Photo p) {
		captionTF.setText(p.getName());
		dateTimeTF.setText(p.getDateModified().toString());
		tagsTA.clear();
		int i = 0;
		for(Tag tag : p.getTags()) {
			if(i > 0) tagsTA.appendText("\n");
			tagsTA.appendText(tag.toString());
			i++;
		}
	}
}
