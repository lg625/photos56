package controller;

import java.io.File;
import java.net.MalformedURLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Optional;

import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import model.Tag;
import javafx.stage.Stage;
/**
 * Controller for AddPhoto
 * @author Jon Cobi Delfin & Luis Guzman
 *
 */
public class AddPhotoController {
	@FXML Text albumNameText;
	@FXML TextField photoNameTF, tagsTF, tagValueTF;
	@FXML ImageView imageView;
	@FXML Button addPhotoBtn, cancelBtn, addTagBtn, selectFileBtn;
	@FXML MenuButton tagValueMenuBtn;
	@FXML FlowPane tagFlowPane;
	
	PhotosViewController parent;
	String imgURL;
	LocalDateTime imgDate;
	ArrayList<Tag> tags = new ArrayList<Tag>();
	
	@FXML
    private void initialize() {
		tagValueMenuBtn.getItems().clear();
		
		// Initialize Menu Buttons
		for(String s : Tag.defaultTagNames) {
			// Create Menu Item
			MenuItem m1 = new MenuItem(s);
			
			// Set action
			if(s.equals("Add New Value...")) {
				m1.setOnAction(e -> {
					TextInputDialog textPopUp = new TextInputDialog();
					textPopUp.setTitle("Create new Tag Value:");
					textPopUp.setHeaderText("");
					textPopUp.setContentText("Tag Value: ");
					Optional<String> input = textPopUp.showAndWait();
					
					if(input.isPresent()) {
						if(input.get().isEmpty()) {
							Alert alert = new Alert(AlertType.ERROR);
							alert.setContentText("Error: Label name cannot be empty");
							alert.show();
							return;
						}
						else if(Tag.defaultTagNames.contains(input.get().trim())) {
							Alert alert = new Alert(AlertType.ERROR);
							alert.setContentText("Error: Duplicate label name");
							alert.show();
							return;
						}
						
						// Insert new tag name and re-initialize
						Tag.defaultTagNames.add(Tag.defaultTagNames.size()-1, input.get().trim());
						initialize();
						tagValueMenuBtn.setText(input.get().trim());
					}
				});
			}
			else {
				m1.setOnAction(e -> {
					tagValueMenuBtn.setText(m1.getText());
				});
			}
			
			tagValueMenuBtn.getItems().add(m1);
		}
		
        // Set the action for the button
		addPhotoBtn.setOnAction(event -> addPhoto());
    }

	/**
	 * Function occurs when "Add Photo" button is clicked.
	 * Retrieve user input and send new photo data to parent window scene
	 */
	public void addPhoto() {
		// Get information from window scene
		String photoName = photoNameTF.getText();
		
		// Pass information to parent
		parent.passNewPhotoInformation(photoName, imgURL, imgDate, tags);
		
		// Close window scene
		Stage stage = (Stage) addPhotoBtn.getScene().getWindow();
	    stage.close();
	}
	
	/**
	 * Closes window when clicked
	 */
	public void cancel() {
		Stage stage = (Stage)cancelBtn.getScene().getWindow();
		stage.close();
	}
	
	/**
	 * Function called when "Select File" button is clicked
	 * Prompts user to select image file from file explorer
	 * @throws MalformedURLException Throws Exception in the case where URL is not good
	 */
	public void selectFileClicked() throws MalformedURLException {
		// Create FileChooser object and declare filters
		FileChooser fileChooser = new FileChooser();
		fileChooser.getExtensionFilters().add(new ExtensionFilter("Image Files", "*.png", "*.jpg", "*.gif"));
		
		// Show File Explorer
		Stage stage = (Stage)selectFileBtn.getScene().getWindow();
		File file = fileChooser.showOpenDialog(stage);
		
		if(file != null) {
			SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
			String dateInString = sdf.format(file.lastModified());
			try {
				imgDate = sdf.parse(dateInString).toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			// Create new Image and set it
			imgURL = file.toURI().toURL().toString();
			Image img = new Image(imgURL, 200, 150, true, true);
			imageView.setImage(img);
		}
		else {
//			System.out.println("Error getting file");
		}
	}
	
	/**
	 * Function called when "Add Tag" button is clicked
	 * Inserts new Tag object and calls displayTags()
	 */
	public void onAddTagBtnClick() {
		if(tagValueTF.getText().trim().isEmpty()) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setContentText("Error: value cannot be empty");
			alert.show();
			return;
		}
		
		Tag newTag = new Tag(tagValueMenuBtn.getText(), tagValueTF.getText().trim());
		tags.add(newTag);
		displayTags();
	}
	
	/**
	 * Properly display tags within ArrayList<Tag> into GUI
	 */
	private void displayTags() {
		tagFlowPane.getChildren().clear();
		
		// For each tag, add into FlowPane GUI
		for(Tag tag : tags) {
			HBox tagBox = new HBox();
			Text tagName = new Text(tag.tagName);
			Text tagValue = new Text(tag.tagValue);
			Button removeBtn = new Button("X");
			
			HBox.setMargin(tagName, new Insets(5, 5, 0, 10));
			
			// Set action for remove tag button
			removeBtn.setOnAction(e -> {
				for(Tag t : tags) {
					if(t.tagName.equals(tagName.getText())) {
						tags.remove(t);
						break;
					}
				}
				
				displayTags();
			});
			
			tagBox.getChildren().addAll(tagName, tagValue, removeBtn);
			tagFlowPane.getChildren().add(tagBox);
		}
	}
	
	/**
	 * Function used when transferring data between consoles.
	 * @param parent Reference to its parent controller
	 */
	public void transferData(PhotosViewController parent) {
		this.parent = parent;
	}
}
