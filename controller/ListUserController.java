package controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Optional;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import model.UserIO;

/**
 * Controller for the ListUsers view
 * @author Jon Cobi Delfin and Luis Guzman
 *
 */
public class ListUserController {
	private static ArrayList<String> users;
	private static final String storeDir = UserIO.class.getResource("/dat").getPath() + "/";


	private String selectedUser;
	private int selectedUserIndex;
	
	@FXML private Button goBackBtn;
	@FXML private Button createUserBtn;
	@FXML private Button deleteUserBtn;
	@FXML private ListView<String> userList; 
	
	
	/**
	 * Initialize the view. Loads in user data.
	 */
	@FXML
	private void initialize() {
		//Load the user data
		try {
			users = (ArrayList<String>)(readUserList());
		}catch (Exception e) {
//			System.out.println("Error reading in user data.");
			e.printStackTrace();
			System.exit(0);
		}
		//Display list of users
		for(String s : users) {
			userList.getItems().add(s);
		}
	}
	
	/**
	 * Go back to viewing albums. Admin will still be logged in as admin and the user data will be written to disk.
	 */
	public void goBack() {
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/AlbumsView.fxml"));
			Stage stage = (Stage)goBackBtn.getScene().getWindow(); 
			Scene scene = new Scene(loader.load());
			AlbumsViewController controller = (AlbumsViewController)loader.getController();
			controller.ListUsersPageToAlbumsViewPage();
			
			stage.setTitle("Your Photos");
			stage.setScene(scene);
			scene.getWindow().setOnCloseRequest(null); //Remove the handler
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
	
	/**
	 * Create a new user.
	 */
	public void createUser() {
		//Prompt for input
		TextInputDialog newUser = new TextInputDialog();
		newUser.setTitle("Create new user");
		newUser.setHeaderText("");
		newUser.setContentText("User name: ");
		Optional<String> input = newUser.showAndWait();
		
		if(users.contains(input.get())) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setContentText("User already exists.");
			alert.show();
			return;
		}
		
		//Add new user to the lists
		users.add(input.get());
		userList.getItems().add(input.get());
		//Add user to /dat
		try {
			File f = new File(storeDir + File.separator + input.get() + ".dat");
			f.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	/**
	 * Delete a user.
	 */
	public void deleteUser() {
		if(users.isEmpty()) return;
		users.remove(selectedUser);
		userList.getItems().remove(selectedUserIndex);
		deleteUserBtn.setVisible(false);
		
		//Remove from /dat
		File f = new File(storeDir + File.separator + selectedUser);
		f.delete();
	}
	
	/**
	 * Get the contents of the selected row.
	 */
	public void userListClicked() {
		selectedUser = userList.getSelectionModel().getSelectedItem();
		selectedUserIndex = userList.getSelectionModel().getSelectedIndex();
		if(selectedUserIndex != -1) deleteUserBtn.setVisible(true);
	}
	
	/**
	 * Users and their data are stored in the /dat directory.
	 * Read the prefixes and pass them off as users. We can then delete them and all their data
	 * in one action by deleting their file.
	 * @return
	 */
	private static final ArrayList<String> readUserList(){
		ArrayList<String> ret = new ArrayList<>();
		File datDir = new File(storeDir);
		for(File entry : datDir.listFiles()) {
			String add = entry.getName().split(".dat")[0];
			if(!add.equals("stock") && !add.equals("admin")) {
				ret.add(add);
			}
		}
		return ret;
	}
	
}
