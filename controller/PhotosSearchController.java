package controller;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import model.Album;
import model.Photo;
import model.Tag;

/**
 * Controller for PhotosSearch
 * @author Jon Cobi Delfin and Luis Guzman
 *
 */
public class PhotosSearchController {
	@FXML FlowPane tagContainerFlowPane;
	@FXML TextField tagValueTF, newAlbumNameTF;
	@FXML Button goBackBtn, addTagBtn, searchTagsBtn, createAlbumBtn;
	@FXML VBox radioButtonContainerVBox;
	@FXML RadioButton andRadioBtn, orRadioBtn;
	@FXML GridPane thumbnailsGP;
	@FXML DatePicker minDateDP, maxDateDP;
	@FXML MenuButton tagNameMenuBtn;
	
	// ArrayList containing all user photos
	ArrayList<Photo> allUserPhotos = new ArrayList<Photo>();
	// ArrayList holding date ranges
	ArrayList<LocalDateTime> dateRange = new ArrayList<LocalDateTime>();
	// ArrayList containing tags
	ArrayList<String> tagSearch = new ArrayList<String>();
	ArrayList<Tag> tagSearchObjects = new ArrayList<Tag>();
	// ArrayList containing Photos that match search parameters
	ArrayList<Photo> searchResult = new ArrayList<Photo>();
	// String containing either "AND" or "OR", "OR" by default
	String tagSearchType = "OR";
	
	String user;
	ArrayList<Album> userAlbums;
	
	@FXML
    private void initialize() {
		if(userAlbums == null)
			return;
		
		// For all user albums, add them into allUserPhotos
		for(Album a : userAlbums) {
			for(Photo p : a.photos) {
				allUserPhotos.add(p);
				searchResult.add(p);
			}
		}
		
		displayPhotos(allUserPhotos);
		
		// Hide tag search radio buttons and group them together
		radioButtonContainerVBox.setVisible(false);
		searchTagsBtn.setVisible(false);
		ToggleGroup radioBtnGroup = new ToggleGroup();
		orRadioBtn.setToggleGroup(radioBtnGroup);
		andRadioBtn.setToggleGroup(radioBtnGroup);
	
		// Initialize Menu Buttons
		for(String s : Tag.defaultTagNames) {
			// Create Menu Item
			MenuItem m1 = new MenuItem(s);
			m1.setOnAction(e -> {
				tagNameMenuBtn.setText(m1.getText());
			});
			tagNameMenuBtn.getItems().add(m1);
		}
		
        // Set the action for the button
		addTagBtn.setOnAction(event -> addTag());
    }
	
	/**
	 * Reset GUI parameters
	 * @param e ActionEvent
	 */
	@FXML
	public void onResetBtnClick(ActionEvent e) {
		// Reset everything
		minDateDP.setValue(null);
		maxDateDP.setValue(null);
		
		tagSearchType = "OR";
		
		radioButtonContainerVBox.setVisible(false);
		searchTagsBtn.setVisible(false);
		
		tagValueTF.setText(null);
		tagSearch.clear();
		displayTags();
		
		searchResult.clear();
		searchResult = allUserPhotos;
		displayPhotos(allUserPhotos);
	}
	
	/**
	 * Actions taken when search photos by date occurs
	 * Resets any search by tags
	 * @param e ActionEvent
	 */
	public void onSearchDatesBtnClick(ActionEvent e) {
		// Reset all tags
		radioButtonContainerVBox.setVisible(false);
		searchTagsBtn.setVisible(false);
		tagValueTF.setText(null);
		tagSearch.clear();
		tagSearchType = "OR";
		displayTags();
		
		// Set min date range
		if(minDateDP.getValue() == null) {
			Alert prompt = new Alert(AlertType.ERROR);
			prompt.setContentText("No start date selected!");
			prompt.show();
			return;
		}
		else
			dateRange.add(0, minDateDP.getValue().atStartOfDay());
		
		// Set max date range
		if(maxDateDP.getValue() == null)
			dateRange.add(1, LocalDateTime.now());
		else
			dateRange.add(1, maxDateDP.getValue().atStartOfDay());
		
		searchResult.clear();
		// Search through photos for images that are within range
		for(Photo p : allUserPhotos) {
			if((p.getDateModified().compareTo(dateRange.get(0)) >= 0) && (p.getDateModified().compareTo(dateRange.get(1)) <= 0)){
				searchResult.add(p);
			}
		}
		
		displayPhotos(searchResult);
	}
	
	/**
	 * Actions taken when search phots by tags occurs
	 * Resets any search by date
	 * @param e ActionEvent
	 */
	public void onSearchTagsClick(ActionEvent e) {
		// Remove all date search features
		minDateDP.setValue(null);
		maxDateDP.setValue(null);
		
		searchResult.clear();
		
		// Display all photos if no tags are in
		//if(tagSearch.size() == 0) {
		if(tagSearchObjects.size() == 0) {
			tagSearchType = "OR";
			displayPhotos(allUserPhotos);
			return;
		}
		
		// Search tags within photos
		for(Photo p : allUserPhotos) {
			System.out.println(p.getName() + ": tag = " + p.getTags().get(0).tagName + " " + p.getTags().get(0).tagValue);

			if(tagSearchType == "AND" && p.getTags().containsAll(tagSearchObjects)) {
				searchResult.add(p);
			}
			else if (tagSearchType == "OR") {
				// search if at least one searching tag exists within photo tag
				for(Tag tag : tagSearchObjects) {
					if(p.getTags().contains(tag)) {
						searchResult.add(p);
						break;
					}
					System.out.println("\tDoes not contain: " + tag.tagName + " " + tag.tagValue);
				}
			}
		}
		
		displayPhotos(searchResult);
	}
	
	/**
	 * Function runs search when "OR" button is selected
	 * @param e ActionEvent
	 */
	@FXML
	public void onOrBtnClick(ActionEvent e) {
		tagSearchType = "OR";
		onSearchTagsClick(null);
	}
	
	/**
	 * Function runs search when "AND" button is selected
	 * @param e ActionEvent
	 */
	@FXML
	public void onAndBtnClick(ActionEvent e) {
		tagSearchType = "AND";
		onSearchTagsClick(null);
	}
	
	/**
	 * Change views to display previous window scene
	 * @param e ActionEvent
	 */
	public void onGoBackBtnClick(ActionEvent e) {
		//Change views
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/AlbumsView.fxml"));
			Parent parent = loader.load();
			
			//Create instance to pass user and specified album
			AlbumsViewController controller = (AlbumsViewController) loader.getController();
			controller.photosViewPageToAlbumsViewPage(user, userAlbums);

			Stage stage = (Stage)goBackBtn.getScene().getWindow();
			Scene scene = new Scene(parent);
			stage.setTitle("Your Albums");
			stage.setScene(scene);
			scene.getWindow().setOnCloseRequest(null);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
	
	/**
	 * Actions taken when user clicks on "Create New Album" button.
	 * @param e ActionEvent
	 */
	public void onCreateAlbumBtnClick(ActionEvent e) {
		// Create new album object
		Album newAlbum = new Album(user, newAlbumNameTF.getText().trim());
		for(Photo p : searchResult) {
			newAlbum.addPhoto(p);
		}

		// Insert object into ArrayList<Album>
		userAlbums.add(newAlbum);

		// Display individual album view, change views
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/PhotosView.fxml"));
			Parent parent = loader.load();
			
			//Create instance to pass user and specified album
			PhotosViewController controller = (PhotosViewController) loader.getController();
			controller.initAlbum(user, userAlbums, newAlbum.name);

			Stage stage = (Stage)newAlbumNameTF.getScene().getWindow();
			Scene scene = new Scene(parent);
			stage.setTitle("View Album - " + newAlbum.name);
			stage.setScene(scene);
			scene.getWindow().setOnCloseRequest(null);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
	
	/**
	 * Function runs when "Add Tag" button is clicked.
	 */
	private void addTag() {
		String tagName = tagNameMenuBtn.getText(), tagValue = tagValueTF.getText().trim();
		Tag t = new Tag(tagName, tagValue);
		
		if(tagSearchObjects.contains(t) || tagValue.length() == 0) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setContentText("Invalid tag");
			alert.show();
		}
		else {
			tagSearchObjects.add(t);
			displayTags();		
			onSearchTagsClick(null);
		}
	}
	
	/**
	 * Function used to display photos onto GUI GridPane
	 */
	private void displayPhotos(ArrayList<Photo> photoList) {
		thumbnailsGP.getChildren().clear();
		
		int row = 0, col = -1, maxCol = 3;
		// For each photo within the album, add into grid view
		for(Photo p : photoList) {	
			row = col == maxCol-1 ? ++row : row;
			col = col == maxCol-1 ? 0 : ++col;
			//System.out.println("row = " + row + " col = " + col);
			
			VBox box = new VBox();
			box.setSpacing(10);
			box.setCenterShape(true);
			
			ImageView image = new ImageView(new Image(p.getImageURL()));
			image.setFitWidth(150);
			image.setFitHeight(150);
			image.setPreserveRatio(true);
			Text photoName = new Text(p.getName());
			
			VBox.setMargin(image, new Insets(50, 10, 0, 10));
			VBox.setMargin(photoName, new Insets(5, 10, 20, 10));
			
			box.getChildren().addAll(image, photoName);
			
			thumbnailsGP.add(box, col, row);
		}
	}
	
	/**
	 * Function properly displays current tags being searched for
	 */
	private void displayTags() {
		tagContainerFlowPane.getChildren().clear();
		
		// Enable or disable Tag Search Button
		if(tagSearchObjects.size() >= 1)
			searchTagsBtn.setVisible(true);
		else
			searchTagsBtn.setVisible(false);
		
		// Enable or disable Radio Buttons
		if(tagSearchObjects.size() >= 2)
			radioButtonContainerVBox.setVisible(true);
		else
			radioButtonContainerVBox.setVisible(false);
		
		// For each tag, add into FlowPane GUI
		//for(String tag : tagSearch) {
		for(Tag tag : tagSearchObjects) {
			HBox tagBox = new HBox();
			Text tagName = new Text(tag.tagName);
			Text tagValue = new Text(tag.tagValue);
			Button removeBtn = new Button("X");
			
			HBox.setMargin(tagName, new Insets(5, 5, 0, 10));
			
			// Set action for remove tag button
			removeBtn.setOnAction(e -> {
				for(Tag t : tagSearchObjects) {
					if(t.tagName.equals(tagName.getText())) {
						tagSearchObjects.remove(t);
						break;
					}
				}
				
				displayTags();
				onSearchTagsClick(null);
			});
			
			tagBox.getChildren().addAll(tagName, tagValue, removeBtn);
			tagContainerFlowPane.getChildren().add(tagBox);
		}
	}
	
	/**
	 * Function used to transfer data between window scenes
	 * @param user String
	 * @param userAlbums ArrayList of Album Objects
	 */
	public void receiveAlbum(String user, ArrayList<Album> userAlbums) {
		this.user = user;
		this.userAlbums = userAlbums;
		initialize();
	}
}
