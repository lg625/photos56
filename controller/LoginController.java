package controller;

import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;

/**
 * Controller for the Login Screen view
 * @author Jon Cobi Delfin and Luis Guzman
 *
 */

public class LoginController {
	@FXML TextField UsernameTF;
	@FXML Button loginBtn;
	
	
	@FXML
	public void initialize() {
		UsernameTF.setText("");
	}
	
	/**
	 * Actions taken when user click "Login" button
	 * @param e ActionEvent
	 */
	public void onLoginClick(ActionEvent e) {
		if(UsernameTF.getText().length() == 0) {
			Alert prompt = new Alert(AlertType.ERROR);
			prompt.setContentText("Please enter username!");
			prompt.show();
		}
		else {
			// Perform regular user functionality
			try {
				FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/AlbumsView.fxml"));
				Parent parent = loader.load();
				
				AlbumsViewController controller = (AlbumsViewController) loader.getController();
				controller.loginPageToAlbumsViewPage(UsernameTF.getText().trim());
				
				Stage stage = (Stage)loginBtn.getScene().getWindow(); 
				Scene scene = new Scene(parent);
				stage.setScene(scene);
				stage.setTitle("Your Albums");
				scene.getWindow().setOnCloseRequest(null); //Remove the handler
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}
}
