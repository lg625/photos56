package controller;

import java.io.EOFException;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Optional;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextInputDialog;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import model.Album;
import model.Photo;
import model.Tag;

/**
 * Controller for AlbumsView
 * @author Jon Cobi Delfin and Luis Guzman
 *
 */
public class AlbumsViewController {
	private int rowCount = 0;
	private ArrayList<Album> albums;
	private String user;
	private String userFile;

	@FXML private Button adminBtn;
	@FXML private Button createBtn;
	@FXML private Button logoutBtn;
	@FXML private Button searchPhotoBtn;
	@FXML private GridPane albumList;
	@FXML private Text usernameText;
	
	@SuppressWarnings("unchecked")
	@FXML
	private void initialize() {
		// Only run once user is initialized
		if(user == null)
			return;
		
		usernameText.setText(user);
		Platform.runLater(() -> albumList.getScene().getWindow().setOnCloseRequest(e -> {
			try {
				model.UserIO.writeUserData(userFile, albums);
			} catch(IOException e1) {
				e1.printStackTrace();
			}
		}));
		
		//Read in user album data
		try {
			userFile = user + ".dat";
			albums = (ArrayList<Album>)model.UserIO.readUserData(userFile);
		} catch(EOFException e) {
			albums = new ArrayList<>();
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(0);
		}
		
		if(user.equals("stock")) genStockPhotos();
		
		for(Album a : albums) {
			String s = a.name;
			RowConstraints rc = new RowConstraints(50);
			albumList.getRowConstraints().add(rc);
			Button b1 = new Button("View");
			b1.setOnAction(e -> viewAlbum(e));
			Button b2 = new Button("Rename");
			b2.setOnAction(e -> renameAlbum(e));
			Button b3 = new Button("Delete");
			b3.setOnAction(e -> deleteAlbum(e));
			albumList.add(new Label(s), 0, rowCount);
			albumList.add(new Label(Integer.toString(a.numPhotos)), 1, rowCount);
			String dr = a.numPhotos == 0 ? "n/a to n/a" : a.getDateRange()[0] + " to " + a.getDateRange()[1];
			albumList.add(new Label(dr), 2, rowCount);
			albumList.add(b1, 3, rowCount);
			albumList.add(b2, 4, rowCount);
			albumList.add(b3, 5, rowCount);
			rowCount++;
		}
	}
	
	/**
	 * Change to admin view. Only available if the admin is logged in.
	 */
	public void adminView() {
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/ListUsers.fxml"));
			Stage stage = (Stage)adminBtn.getScene().getWindow();
			Scene scene = new Scene(loader.load());

			stage.setTitle("Admin");
			stage.setScene(scene);
			scene.getWindow().setOnCloseRequest(null);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
	
	/**
	 * Create a new album.
	 * @throws IOException Throws IOException in the case where file does not exist
	 */
	public void createAlbum() throws IOException {
		//Prompt for input
		TextInputDialog newAlbum = new TextInputDialog();
		newAlbum.setTitle("Create new album");
		newAlbum.setHeaderText("");
		newAlbum.setContentText("Album name: ");
		Optional<String> input = newAlbum.showAndWait();
		if(input.isPresent()) {
			if(input.get().isEmpty()) {
				Alert alert = new Alert(AlertType.ERROR);
				alert.setContentText("Error: Album name cannot be empty");
				alert.show();
				return;
			}else if(isDuplicateName(input.get())) {
				Alert alert = new Alert(AlertType.ERROR);
				alert.setContentText("Error: Duplicate album name");
				alert.show();
				return;
			}
			//Add to gridpane
			Button b1 = new Button("View");
			b1.setOnAction(e -> viewAlbum(e));
			Button b2 = new Button("Rename");
			b2.setOnAction(e -> renameAlbum(e));
			Button b3 = new Button("Delete");
			b3.setOnAction(e -> deleteAlbum(e));
			
			RowConstraints rc = new RowConstraints(50);
			albumList.getRowConstraints().add(rc);
			albumList.add(new Label(input.get()), 0, rowCount);
			albumList.add(new Label("0"), 1, rowCount);
			albumList.add(new Label("n/a to n/a"), 2, rowCount);
			albumList.add(b1, 3, rowCount);
			albumList.add(b2, 4, rowCount);
			albumList.add(b3, 5, rowCount);
			rowCount++;
			
			//Add to albums
			Album a = new Album(user, input.get(), new ArrayList<Photo>());
			albums.add(a);
			
			writeToFile();
		}
	}
	
	/**
	 * View an album
	 * @param e ActionEvent from button click
	 */
	public void viewAlbum(ActionEvent e) {
		//Get the source of the button
		Node source = (Node)e.getSource();
		int row = GridPane.getRowIndex(source).intValue(); //row to view
		String albumName = ((Label)albumList.getChildren().get(row*6)).getText();
		
		//Change views
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/PhotosView.fxml"));
			Parent parent = loader.load();
			
			//Create instance to pass user and specified album
			PhotosViewController controller = (PhotosViewController) loader.getController();
			controller.initAlbum(user, albums, albumName);

			Stage stage = (Stage)albumList.getScene().getWindow();
			Scene scene = new Scene(parent);
			stage.setTitle("View Album - " + albumName);
			stage.setScene(scene);
			scene.getWindow().setOnCloseRequest(null);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		writeToFile();
	}
	
	/**
	 * Rename an album
	 * @param e ActionEvent from button click
	 */
	public void renameAlbum(ActionEvent e) {
		//Get the source of the button
		Node source = (Node)e.getSource();
		int row = GridPane.getRowIndex(source).intValue(); //row to change
		
		//Prompt the user for new name
		TextInputDialog input = new TextInputDialog();
		input.setContentText("Enter new name");
		input.setHeaderText("Rename");
		Optional<String> newName = input.showAndWait();
		if(newName.isPresent()) {
			if(newName.get().isEmpty()) {
				Alert alert = new Alert(AlertType.ERROR);
				alert.setContentText("Error: Album name cannot be empty");
				alert.show();
				return;
			}else if(isDuplicateName(newName.get())) {
				Alert alert = new Alert(AlertType.ERROR);
				alert.setContentText("Error: Duplicate album name");
				alert.show();
				return;
			}
			
			//Change name in gridpane
			((Label)albumList.getChildren().get(row*6)).setText(newName.get());
			
			//Change name in albums
			albums.get(row).setName(newName.get());
			
			writeToFile();
		}
	}
	
	/**
	 * Remove an album
	 * @param e ActionEvent from button click
	 */
	public void deleteAlbum(ActionEvent e) {
		//Get the source of the button
		Node source = (Node)e.getSource();
		int row = GridPane.getRowIndex(source).intValue(); //row to delete
		String name = ((Label)albumList.getChildren().get(row*6)).getText();
		//Alert the user
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setContentText("Are you sure you want to delete " + name + "?");
		Optional<ButtonType> result = alert.showAndWait();
		ArrayList<Node> d = new ArrayList<>();
		if(result.get() == ButtonType.OK) {
			//Iterate through the children
			boolean del = true;
			for(Node child : albumList.getChildren()) {
				int cur = GridPane.getRowIndex(child) == null ? 0 : GridPane.getRowIndex(child).intValue();
				if(del && cur == row) {
					for(int i = 0; i < 6; i++) {
						d.add(albumList.getChildren().get(cur*6+i));
					}
					del = false;
				}else if(cur > row) {
					//Need to shift the rows down
					GridPane.setRowIndex(child, cur-1);
				}
			}
			//Delete the row
			albumList.getRowConstraints().remove(row);
			albumList.getChildren().removeAll(d);
			albums.remove(row);
			rowCount--;
			
		}
		
		writeToFile();
	}
	
	/**
	 * Log the user out. Write the data to the disk.
	 */
	public void logout() {
		try {
			writeToFile();
			
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/Login.fxml"));
			Stage stage = (Stage)logoutBtn.getScene().getWindow(); 
			Scene scene = new Scene(loader.load());

			stage.setTitle("Photos - User Login");
			stage.setScene(scene);
			scene.getWindow().setOnCloseRequest(null);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
	
	/**
	 * Pass user name data from login page to albums view page
	 * @param user Name of user
	 */
	public void loginPageToAlbumsViewPage(String user) {
		this.user = user;
		if(user.equals("admin")) adminBtn.setVisible(true);
		initialize();
	}
	
	/**
	 * Pass back data from PhotosView to here
	 * @param user Name of user
	 * @param userAlbums ArrayList of Album objects
	 */
	public void photosViewPageToAlbumsViewPage(String user, ArrayList<Album> userAlbums) {
		this.user = user;
		this.albums = userAlbums;
		userFile = user + ".dat"; //userfile becomes null after transition, need to redefine
		writeToFile(); //write the changes to file
		initialize();
	}
	
	/**
	 * Resets user to 'admin' when clicking go back from Admin View
	 */
	public void ListUsersPageToAlbumsViewPage() {
		this.user = "admin";
		adminBtn.setVisible(true);
		initialize();
	}
	
	/**
	 * Redirects user to search view.
	 * @param e ActionEvent
	 */
	public void searchPhotoBtnClick(ActionEvent e) {
		//Change views
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/PhotosSearch.fxml"));
			Parent parent = loader.load();
			
			//Create instance to pass user and specified album
			PhotosSearchController controller = (PhotosSearchController) loader.getController();
			controller.receiveAlbum(user, albums);

			Stage stage = (Stage)searchPhotoBtn.getScene().getWindow();
			Scene scene = new Scene(parent);
			stage.setTitle("Search Photos");
			stage.setScene(scene);
			scene.getWindow().setOnCloseRequest(null);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
	
	/**
	 * Check if the stock album exists. Create if not.
	 */
	private void genStockPhotos() {
		//Check if stock is already generated
		for(Album a : albums) {
			if(a.name.equals("stock")) return;
		}
		
		ArrayList<Photo> stockPhotos = new ArrayList<>();		
		File stockDir = null;
		try {
			//System.out.println("Stock path = " + AlbumsViewController.class.getResource("/stock").toURI());
			stockDir = new File(AlbumsViewController.class.getResource("/stock").toURI());
		} catch (URISyntaxException e1) {
			e1.printStackTrace();
		}
		
		for(File f : stockDir.listFiles()) {
			SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
			String dateInString = sdf.format(f.lastModified());
			LocalDateTime imgDate = null;
			try {
				imgDate = sdf.parse(dateInString).toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
			} catch(Exception e) {
				e.printStackTrace();
			}
			String imgURL = f.toURI().toString();
			
			Photo p = new Photo(imgURL, "stock", "stock", imgDate, new ArrayList<Tag>());
			stockPhotos.add(p);
		}
		
		albums.add(new Album("stock", "stock", stockPhotos));
	}
	
	/**
	 * Checks if the input album name is a duplicate
	 * @param name
	 * @return
	 */
	private boolean isDuplicateName(String albumName) {
		for(Album a : albums) {
			if(a.name.equals(albumName)) return true; //duplicate
		}
		return false; //no duplicate
	}
	
	/**
	 * Write to file
	 */
	private void writeToFile() {
		try {
			model.UserIO.writeUserData(userFile, albums);
		} catch(IOException e1) {
			e1.printStackTrace();
		}
	}
}
