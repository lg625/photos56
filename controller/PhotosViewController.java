package controller;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import model.Album;
import model.Photo;
import model.Tag;

/**
 * Controller for PhotosView
 * @author Jon Cobi Delfin and Luis Guzman
 *
 */
public class PhotosViewController {	
	@FXML Button goBackBtn, addPhotoBtn, viewPhotoBtn, deletePhotoBtn;
	@FXML MenuButton movePhotoMenuBtn, copyPhotoMenuBtn;
	@FXML Text albumNameText;
	@FXML GridPane thumbnailsGP;
	
	ArrayList<Album> userAlbums;
	Album album;
	String user, albumName;
	
	Node selectedCell = null;
	int selectedCellRow, selectedCellCol;
	
	@FXML
	public void initialize() {
		// Only run once user is initialized
		if(user == null)
			return;
		
		String userFile = user + ".dat";
		Platform.runLater(() -> thumbnailsGP.getScene().getWindow().setOnCloseRequest(e -> {
			try {
				model.UserIO.writeUserData(userFile, userAlbums);
			} catch(IOException e1) {
				e1.printStackTrace();
			}
		}));
		
		albumNameText.setText(albumName);
		displayPhotosInGridPane();
		
		// Initialize Menu Buttons
		for(Album a : userAlbums) {
			if(a.name.equals(albumName))
				continue;
			
			// Create Move To Menu Item
			MenuItem m1 = new MenuItem(a.name);
			m1.setOnAction(e -> {
				movePhoto(m1.getText());
			});
			movePhotoMenuBtn.getItems().add(m1);
			
			MenuItem m2 = new MenuItem(a.name);
			m2.setOnAction(e -> {
				copyPhoto(m2.getText());
			});
			copyPhotoMenuBtn.getItems().add(m2);
		}
	}
	
	/**
	 * Function called when "Add Photo" button is clicked.
	 * Displays new overlay scene window and prompts user to input new photo information
	 * @param e ActionEvent
	 */
	public void displayAddPhotoScene(ActionEvent e) {		
		// Display new overlay window
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/AddNewPhoto.fxml"));
			Parent parent = loader.load();
			
			AddPhotoController controller = (AddPhotoController) loader.getController();
			controller.transferData(this);
			
			Stage stage = new Stage(StageStyle.DECORATED);
			stage.setTitle("Add New Photo");
			stage.setScene(new Scene(parent));
			stage.show();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
	
	/**
	 * Function called whenever "View" button is clicked
	 * Displays new window scene with a detailed view of photo selected
	 * @param e ActionEvent
	 */
	public void viewPhotoDetails(ActionEvent e) {
		//Photo to view
		model.Photo p;
		int index = selectedCellRow * 4 + selectedCellCol;
		p = album.photos.get(index);
		
		// Display new overlay window
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/PhotoInfoView.fxml"));
			Parent parent = loader.load();
			
			PhotoInfoViewController controller = (PhotoInfoViewController) loader.getController();
			
			Stage stage = new Stage(StageStyle.DECORATED);
			controller.initData(album, p, stage);
			stage.setTitle("Photo");
			stage.setScene(new Scene(parent));
			stage.showAndWait();
			
			//Re display photos in case of deletion
			displayPhotosInGridPane();
			
			//Hide photo option buttons
			viewPhotoBtn.setVisible(false);
			deletePhotoBtn.setVisible(false);
			movePhotoMenuBtn.setVisible(false);
			copyPhotoMenuBtn.setVisible(false);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
	}
	
	/**
	 * Function called whenever "Delete" button is clicked.
	 * Removes selected Photo from Album and updates GridPane display
	 * @param e ActionEvent
	 */
	public void deletePhoto(ActionEvent e) {
		// Delete photo from Album object
		int photoIndex = selectedCellRow * 4 + selectedCellCol, albumIndex = userAlbums.indexOf(album);
		album.photos.remove(photoIndex);
		album.numPhotos--;
		userAlbums.set(albumIndex, album);
		
		// Update photos from GridPane
		displayPhotosInGridPane();
		
		// Hide photo option buttons
		viewPhotoBtn.setVisible(false);
		deletePhotoBtn.setVisible(false);
		movePhotoMenuBtn.setVisible(false);
		copyPhotoMenuBtn.setVisible(false);
	}
	
	/**
	 * Function is called whenever "Go Back" button is clicked on.
	 * Returns to previous window scene
	 * @param e ActionEvent
	 */
	public void goBack(ActionEvent e) {		
		//Change views
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/AlbumsView.fxml"));
			Stage stage = (Stage)goBackBtn.getScene().getWindow();
			Scene scene = new Scene(loader.load());
			
			//Create instance to pass user and specified album
			AlbumsViewController controller = (AlbumsViewController) loader.getController();
			controller.photosViewPageToAlbumsViewPage(user, userAlbums);

			stage.setTitle("Your Albums");
			stage.setScene(scene);
			scene.getWindow().setOnCloseRequest(null);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
	
	/**
	 * Function called whenever a GirdPane cell is selected.
	 * Displays "View" & "Delete" buttons
	 */
	private void displayPhotoOptions() {
		// Update selected cell row and col
        selectedCellRow = GridPane.getRowIndex(selectedCell);
		selectedCellCol = GridPane.getColumnIndex(selectedCell);
    
        // Display photo option buttons
		viewPhotoBtn.setVisible(true);
		deletePhotoBtn.setVisible(true);
		movePhotoMenuBtn.setVisible(true);
		copyPhotoMenuBtn.setVisible(true);
	}

	/**
	 * Function used to pass new photo information. 
	 * Inserts new VBox into GUI GridPane.
	 * @param photoName Name of photo
	 * @param imgURL String of image URL
	 * @param date LocalDateTime object that holds photo date
	 * @param tags ArrayList of tags
	 */
	public void passNewPhotoInformation(String photoName, String imgURL, LocalDateTime date, ArrayList<Tag> tags) {
		if(imgURL == null) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setContentText("Error: Image cannot be null");
			alert.show();
			return;
		}
		
		// Create new Photo object
		Photo p = new Photo(imgURL, user, photoName, date, tags);
		
		// Update album into ArrayList<Album>
		int albumIndex = userAlbums.indexOf(album);
		album.addPhoto(p);
		userAlbums.set(albumIndex, album);
		
		displayPhotosInGridPane();
	}
	
	/**
	 * Display photos in the grid pane
	 */
	private void displayPhotosInGridPane() {		
		thumbnailsGP.getChildren().clear();
		
		int row = 0, col = -1, maxCol = 4;
		// For each photo within the album, add into grid view
		for(Photo p : album.photos) {	
			row = col == maxCol-1 ? ++row : row;
			col = col == maxCol-1 ? 0 : ++col;
			//System.out.println("row = " + row + " col = " + col);
			
			VBox box = new VBox();
			box.setSpacing(10);
			box.setCenterShape(true);
			
			ImageView image = new ImageView(new Image(p.getImageURL()));
			image.setFitWidth(150);
			image.setFitHeight(200);
			image.setPreserveRatio(true);
			Text photoName = new Text(p.getName());
			
			VBox.setMargin(image, new Insets(50, 10, 0, 10));
			VBox.setMargin(photoName, new Insets(5, 10, 20, 10));
			
			box.getChildren().addAll(image, photoName);
			
			thumbnailsGP.add(box, col, row);
			
			// Set OnMouseClicked action
			box.setOnMouseClicked(e -> {
				// Reset style of previous selected cell
				if(selectedCell != null) 
					selectedCell.setStyle("-fx-background-color:#f3f4f4;");

				Node source = (Node)e.getSource();
				selectedCell = source;
				source.setStyle("-fx-background-color:#e8e9e8;");
				
		        // Display buttons
				displayPhotoOptions();
			});
		}
	}
		
	/**
	 * Move photo from album 
	 * @param albumName Target album
	 */
	public void movePhoto(String albumName) {
		Album newAlbum = null;
		// Search for new album
		for(Album a : userAlbums) {
			if(a.name.equals(albumName)) {
				newAlbum = a;
				break;
			}
		}
		
		int photoIndex = selectedCellRow * 4 + selectedCellCol, orgAlbumIndex = userAlbums.indexOf(album), 
				newAlbumIndex = userAlbums.indexOf(newAlbum);
		Photo p = album.photos.get(photoIndex);
		
		// Remove photo from current album
		album.photos.remove(photoIndex);
		album.numPhotos--;
		userAlbums.set(orgAlbumIndex, album);
		
		// Insert photo into new album
		newAlbum.photos.add(p);
		newAlbum.numPhotos++;
		userAlbums.set(newAlbumIndex, newAlbum);
		
		// Hide photo option buttons
		viewPhotoBtn.setVisible(false);
		deletePhotoBtn.setVisible(false);
		movePhotoMenuBtn.setVisible(false);
		copyPhotoMenuBtn.setVisible(false);
		
		displayPhotosInGridPane();
	}
	
	/**
	 * Copy photo to album
	 * @param albumName Target album
	 */
	public void copyPhoto(String albumName) {
		Album newAlbum = null;
		// Search for new album
		for(Album a : userAlbums) {
			if(a.name.equals(albumName)) {
				newAlbum = a;
				break;
			}
		}
		
		int photoIndex = selectedCellRow * 4 + selectedCellCol, newAlbumIndex = userAlbums.indexOf(newAlbum);
		Photo p = album.photos.get(photoIndex);

		// Insert photo into new album
		newAlbum.photos.add(p);
		newAlbum.numPhotos++;
		userAlbums.set(newAlbumIndex, newAlbum);
	}
	
	/**
	 * Function used to pass album data from album overview to album details
	 * @param user Name of user
	 * @param userAlbums ArrayList of Album objects
	 * @param albumName Name of album object
	 */
	public void initAlbum(String user, ArrayList<Album> userAlbums, String albumName) {
		this.user = user;
		this.userAlbums = userAlbums;
		this.albumName = albumName;
		
		// Set album object
		for(Album a : userAlbums) {
			if(a.name.equals(albumName)) {
				album = a;
				break;
			}
		}
		
		initialize();
	}
}
