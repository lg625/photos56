package model;

import java.util.ArrayList;
/**
 * Tag class.
 * @author Jon Cobi Delfin and Luis Guzman
 *
 */
public class Tag implements java.io.Serializable {
	/**
	 * Name of tag
	 */
	public String tagName;
	/**
	 * Tag value
	 */
	public String tagValue;
	/**
	 * Default tag names
	 */
	public static ArrayList<String> defaultTagNames = new ArrayList<String>() {{
		add("Location");
		add("Person");
		add("Add New Value...");
	}};
	
	/**
	 * Tag constructor
	 * @param tagName Name of tag
	 * @param tagValue Value of tag
	 */
	public Tag(String tagName, String tagValue) {
		this.tagName = tagName;
		this.tagValue = tagValue;
	}
	
	@Override
	public boolean equals(Object o) {
		if(!(o instanceof Tag) || o == null) {
			return false;
		}
		
		Tag t = (Tag)o;
		if(this.tagName.equals(t.tagName) && this.tagValue.equals(t.tagValue))
			return true;
		else
			return false;
	}
	
	@Override
	public String toString() {
		return this.tagName + ":" + this.tagValue;
	}
}
