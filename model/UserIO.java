package model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/**
 * Handles reading and writing of data
 * @author Jon Cobi Delfin and Luis Guzman
 *
 */
public class UserIO implements java.io.Serializable{
	private static final String storeDir = UserIO.class.getResource("/dat").getPath() + "/";
	
	/**
	 * Write the user data to the disk.
	 * @param fileName Name of file
	 * @param userData User data in any object format
	 * @throws IOException Throw IOException in the case where file name does not exist
	 */
	public static void writeUserData(String fileName, Object userData)
	throws IOException{
		File f = new File(storeDir + File.separator + fileName);
		if(!f.exists()) f.createNewFile();
		ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(f));
		oos.writeObject(userData);
	}
	
	/**
	 * Read the user data from the disk
	 * @param fileName Name of file
	 * @return Object User data
	 * @throws IOException Throws IOException in the case where file name does not exist
	 * @throws ClassNotFoundException Throws Exception if class does not exist
	 */
	public static Object readUserData(String fileName)
	throws IOException, ClassNotFoundException{
		File f = new File(storeDir + File.separator + fileName);
		if(!f.exists()) f.createNewFile();
		ObjectInputStream ois = new ObjectInputStream(new FileInputStream(f));
		return (Object)(ois.readObject());
	}
	
}
