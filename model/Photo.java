package model;

import java.time.LocalDateTime;
import java.util.*;

/**
 * Photo class. 
 * @author Jon Cobi Delfin and Luis Guzman
 *
 */

public class Photo implements java.io.Serializable {
	/**
	 * Image
	 */
	private String imageURL;
	/**
	 * Name of the photo
	 */
	private String Name;
	/**
	 * Date of last modification. Initial dateModified is the date of creation.
	 */
	private LocalDateTime dateModified;
	/**
	 * List of tags
	 */
	private ArrayList<Tag> tags;
	/**
	 * Owner/publisher name
	 */
	private String owner;
	
	/**
	 * Photo constructor
	 * @param imageURL String format of image URL location
	 * @param owner User who published the photo
	 * @param name Name of the photo
	 * @param dateModified Date of creation
	 * @param tags List of tags
	 */
	public Photo(String imageURL, String owner, String name, LocalDateTime dateModified, ArrayList<Tag> tags) {
		this.imageURL = imageURL;
		this.owner = owner;
		this.Name = name;
		this.dateModified = dateModified;
		this.tags = tags;
	}
	/**
	 * Get image
	 * @return image
	 */
	public String getImageURL() {
		return imageURL;
	}
	
	/**
	 * Get photo owner name
	 * @return owner name
	 */
	public String getOwner() {
		return owner;
	}

	/**
	 * Get the photo name
	 * @return name of the photo
	 */
	public String getName() {
		return Name;
	}
	/**
	 * Set the image
	 * @param i image
	 */
	public void setImageURL(String i) {
		this.imageURL = i;
	}
	/**
	 * Set the photo name
	 * @param n New name 
	 */
	public void setName(String n) {
		this.Name = n;
	}
	/**
	 * Get the date modified of the photo
	 * @return Date last modified of the photo
	 */
	public LocalDateTime getDateModified() {
		return dateModified;
	}
	/**
	 * Set the date last modified of the photo
	 * @param d New date modified
	 */
	public void setDateModified(LocalDateTime d) {
		this.dateModified = d;
	}
	/**
	 * Get the list of photo tags
	 * @return List of photo tags
	 */
	public ArrayList<Tag> getTags(){
		return tags;
	}
	
	/**
	 * Add a tag to a photo
	 * @param t New Tag object
	 * @return True if successfully added. False otherwise.
	 */
	public boolean addTag(Tag t) {
		if(this.tags.contains(t)) {
			System.out.println(t.tagName + " already exists");
			return false;
		}
		this.tags.add(t);
		return true;
	}
	
	/**
	 * Remove specified tag from photo
	 * @param t Target Tag
	 * @return True if successfully removed. False otherwise.
	 */
	public boolean removeTag(Tag t) {
		if(!this.tags.contains(t)) {
			System.out.println(t.tagName + " already exists");
			return false;
		}
		this.tags.remove(t);
		return true;
	}
}
