package model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;

/**
 * Album class. 
 * @author Jon Cobi Delfin and Luis Guzman
 *
 */

public class Album implements Serializable{
	/**
	 * Album name
	 */
	public String name;
	
	/**
	 * Owner of Album
	 */
	public String owner;
	
	/**
	 * Date range of photos within album, index 0 = earliest date; index 1 = latest date
	 */
	public LocalDateTime[] dateRange;
	
	/**
	 * Number of photos within Album
	 */
	public int numPhotos;
	
	/**
	 * Photos within Album
	 */
	public ArrayList<Photo> photos;
	
	/**
	 * Album constructor (2 arg constructor)
	 * @param owner User who published the photo
	 * @param albumName Name of album
	 */
	public Album(String owner, String albumName) {
		this.owner = owner;
		this.name = albumName;
		photos = new ArrayList<Photo>();
		numPhotos = 0;
		dateRange = null;
	}
	
	/**
	 * Album constructor (3 arg constructor)
	 * @param owner User who published the photo
	 * @param albumName Name of album
	 * @param photos List of photos to add to Album
	 */
	public Album(String owner, String albumName, ArrayList<Photo> photos) {
		this.owner = owner;
		this.name = albumName;
		this.photos = photos;
		this.numPhotos = photos.size();
		this.dateRange = getDateRange();
	}
	
	/**
	 * Update album name
	 * @param newName New album name
	 */
	public void setName(String newName) {
		this.name = newName;
	}
	
	/**
	 * Get the date range of photos within Album
	 * @return Date[] Date[0] = earliest date, Date[1] = latest date
	 */
	public LocalDateTime[] getDateRange() {
		if(photos.size() == 0)
			return null;
		
		LocalDateTime minDate = photos.get(0).getDateModified(), maxDate = photos.get(0).getDateModified();
		
		// Go through each photo and update dates
		for(Photo p : photos) {
			if(p.getDateModified().compareTo(minDate) < 0) {
				// p.date comes before minDate
				minDate = p.getDateModified();
			}
			else if(p.getDateModified().compareTo(maxDate) > 0) {
				// p.date comes after maxDate
				maxDate = p.getDateModified();
			}
		}
		
		return new LocalDateTime[] {minDate, maxDate};
	}
	
	/**
	 * Add new photo into Album
	 * @param p Photo object
	 * @return boolean True if successful, False otherwise
	 */
	public boolean addPhoto(Photo p) {
		if(p == null)
			return false;
		
		photos.add(p);
		numPhotos++;
		
		dateRange = getDateRange();
		return true;
	}
	
	/**
	 * Remove photo from photo list
	 * @param p Photo object
	 * @return boolean True if successful, False otherwise
	 */
	public boolean removePhoto(Photo p) {
		if(p == null || !photos.contains(p))
			return false;
		
		photos.remove(p);
		dateRange = getDateRange();
		return true;
	}
	
	@Override
	public String toString() {
		String output = "Album Name: " + name + 
						"\nOwner: " + owner;
		
		if(dateRange != null && photos != null) {
			output = output.concat(
						"\nDate Range: " + dateRange[0] + ", " + dateRange[1] + 
						"\n# of Photos: " + photos.size());
			for(Photo p : photos) {
				output = output.concat("\n\tPhoto Name: " + p.getName() +
									   "\n\tImage URL: " + p.getImageURL());			
			}
		}
		
		return output;
		
	}
}
